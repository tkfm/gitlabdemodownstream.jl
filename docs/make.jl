using GitLabDemoDownstream
using Documenter

makedocs(;
    modules=[GitLabDemoDownstream],
    authors="Takafumi Arakaki <aka.tkf@gmail.com> and contributors",
    repo="https://github.com/tkf/GitLabDemoDownstream.jl/blob/{commit}{path}#L{line}",
    sitename="GitLabDemoDownstream.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://tkf.gitlab.io/GitLabDemoDownstream.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
