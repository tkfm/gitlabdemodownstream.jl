# GitLabDemoDownstream

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://tkf.gitlab.io/GitLabDemoDownstream.jl/dev)
[![Build Status](https://github.com/tkf/GitLabDemoDownstream.jl/badges/master/pipeline.svg)](https://github.com/tkf/GitLabDemoDownstream.jl/pipelines)
[![Coverage](https://github.com/tkf/GitLabDemoDownstream.jl/badges/master/coverage.svg)](https://github.com/tkf/GitLabDemoDownstream.jl/commits/master)
